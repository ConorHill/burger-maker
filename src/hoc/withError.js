import React, { Component } from "react";

import Aux from "./Auxillary";
import Modal from "../components/UI/Modal/Modal";

const withError = (WrapComponent, axios) => {
  return class extends Component {
    state = {
      error: null
    };

    constructor() {
      super();
      this.req = axios.interceptors.request.use(req => {
        this.setState({ error: null });
        return req;
      });

      this.res = axios.interceptors.response.use(res => res, error => {
        this.setState({error: error})
      });
    }

    componentWillUnmount() {
        axios.interceptors.request.eject(this.req);
        axios.interceptors.request.eject(this.res);       
    }
    

    errorHandler = () => {
      this.setState({ error: null });
    };

    render() {
      return (
        <Aux>
          <Modal order={this.state.error} clicked={this.errorHandler}>
            {this.state.error ? this.state.error.message : null}
          </Modal>
          <WrapComponent {...this.props} />
        </Aux>
      );
    }
  };
};

export default withError;
