const contactFormData = {
  name: {
    type: "input",
    config: {
      type: "text",
      placeholder: "Name"
    },
    reqs: {
      required: true
    },
    touched: false,
    valid: false,
    value: ""
  },
  email: {
    type: "input",
    config: {
      type: "email",
      placeholder: "Email Address"
    },
    reqs: {
      required: true
    },
    touched: false,

    valid: false,
    value: ""
  },
  delivery: {
    type: "select",
    config: {
      options: [
        { value: "collection", displayValue: "Collection" },
        { value: "delivery", displayValue: "Delivery" }
      ]
    },
    reqs: {
      required: true
    },
    touched: false,
    valid: false,
    value: "Collection"
  }
};

export default contactFormData;
