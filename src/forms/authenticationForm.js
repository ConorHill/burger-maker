export const authenticationForm = {
  email: {
    type: "input",
    config: {
      type: "email",
      placeholder: "Email Address"
    },
    reqs: {
      required: true
    },
    touched: false,

    valid: false,
    value: ""
  },
  password: {
    type: 'input',
    config: {
      type: 'password',
      placeholder: 'password'
    },
    reqs: {
      required: true,
    },
    touched: false,
    valid: false,
    value: ''
  }
};
