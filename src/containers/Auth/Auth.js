import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { authenticationForm } from "../../forms/authenticationForm";
import { auth, authRedirect } from "../../store/actions/";
import Spinner from "../../components/UI/Spinner/Spinner";
import Input from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import classes from "./Auth.css";

class Auth extends Component {
  state = {
    formControls: authenticationForm,
    signUp: true
  };

  componentDidMount() {
    if(!this.props.building && this.props.authRedirectPath !== '/'){
      this.props.onAuthRedirect();
    }
  }
  

  validCheck(value, reqs) {
    let isValid = true;

    if (reqs.required) {
      isValid = value.trim() !== "";
    }

    return isValid;
  }

  onSumbitHandler = event => {
    event.preventDefault();
    const formValues = {};
    for (let el in this.state.formControls) {
      formValues[el] = this.state.formControls[el].value;
    }
    this.props.onAuth(formValues.email, formValues.password, this.state.signUp);
  };

  onChangeHandler = (event, id) => {
    const updatedFormControls = {
      ...this.state.formControls,
      [id]: {
        ...this.state.formControls[id],
        value: event.target.value,
        valid: this.validCheck(
          event.target.value,
          this.state.formControls[id].reqs
        ),
        touched: true
      }
    };

    this.setState({ formControls: updatedFormControls });
  };

  switchAuthHandler = () => {
    this.setState(prevState => ({ signUp: !prevState.signUp }));
  };

  render() {
    let formData = [];
    for (let data in this.state.formControls) {
      formData.push({
        id: data,
        controls: this.state.formControls[data]
      });
    }

    let form = this.props.loading ? (
      <Spinner />
    ) : (
      formData.map(i => {
        return (
          <Input
            key={i.id}
            elementType={i.controls.type}
            elementConfig={i.controls.config}
            change={event => this.onChangeHandler(event, i.id)}
            invalid={!i.controls.valid}
            touched={i.controls.touched}
            value={i.controls.value}
          />
        );
      })
    );

    let error = this.props.error !== null ? <p>{this.props.error}</p> : null;

    
    let authRedirect = this.props.isAuth ? <Redirect to={this.props.authRedirectPath} /> : null;

    return (
      <div className={classes.AuthForm}>
        {authRedirect}
        <form onSubmit={this.onSumbitHandler}>
          {form}
          {error}
          <Button btnType="continue">
            {this.state.signUp ? "Sign Up" : "Sign In"}
          </Button>
        </form>
        <Button btnType="stop" clicked={this.switchAuthHandler}>
          Switch to {this.state.signUp ? "Sign In" : "Sign Up"}
        </Button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.Auth.loading,
  error: state.Auth.error,
  isAuth: state.Auth.token !== null,
  building: state.Burger.building,
  authRedirectPath: state.Auth.path
});

const mapDispatchToProps = dispatch => ({
  onAuth: (email, password, signUp) => dispatch(auth(email, password, signUp)),
  onAuthRedirect: () => dispatch(authRedirect('/'))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth);
