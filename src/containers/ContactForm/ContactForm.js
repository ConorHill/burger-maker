import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';

import contactFormData from "../../forms/contactFormData";
import Input from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import * as orderActions from '../../store/actions/';

import classes from "./ContactForm.css";
import axiosOrders from "../../axios-orders";

class contactForm extends Component {
  state = {
    formControls: contactFormData,
  };

  validCheck(value, reqs) {
    let isValid = true;

    if (reqs.required) {
      isValid = value.trim() !== "";
    }

    return isValid;
  }

  onSumbitHandler = event => {
    event.preventDefault();
    const formValues = {};
    for(let el in this.state.formControls) {
      formValues[el] = this.state.formControls[el].value;
    }
      const order = {
      ingredients: this.props.ingredients,
      price: this.props.price,
      customer: formValues
    };

    this.props.onProcessOrder(order, this.props.token);
  };

  onChangeHandler = (event, id) => {
    const updatedFormControls = { ...this.state.formControls };
    const updatedFormControl = { ...updatedFormControls[id] };
    updatedFormControl.value = event.target.value;
    updatedFormControl.valid = this.validCheck(
      updatedFormControl.value,
      updatedFormControl.reqs
    );
    updatedFormControl.touched = true;
    updatedFormControls[id] = updatedFormControl;
    this.setState({ formControls: updatedFormControls });
  };

  render() {
    let formData = [];
    for (let data in this.state.formControls) {
      formData.push({
        id: data,
        controls: this.state.formControls[data]
      });
    }

    let form = formData.map(i => {
      return (
        <Input
          key={i.id}
          elementType={i.controls.type}
          elementConfig={i.controls.config}
          change={event => this.onChangeHandler(event, i.id)}
          invalid={!i.controls.valid}
          touched={i.controls.touched}
          value={i.controls.value}
        />
      );
    });

    return (
      <form className={classes.contactForm} onSubmit={this.onSumbitHandler}>
        {form}
        <Button btnType="stop">Cancel</Button>
        <Button btnType="continue">Complete Order</Button>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  ingredients: state.Burger.ingredients,
  price: state.Burger.totalPrice,
  loading: state.Order.loading,
  token: state.Auth.token
})

const mapDispatchToProps = dispatch => ({
  onProcessOrder: (order, token) => {dispatch(orderActions.sendOrder(order, token))}
})


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(contactForm));
