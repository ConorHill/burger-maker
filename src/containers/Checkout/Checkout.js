import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Burger from "../../components/Burger/Burger";
import Spinner from "../../components/UI/Spinner/Spinner";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import ContactForm from "../ContactForm/ContactForm";
import classes from "./Checkout.css";

class Checkout extends Component {

  cancelHandler = () => {
    this.props.history.goBack();
  };

  continuerHandler = () => {
    this.props.history.replace("/checkout/contact");
  };

  render() {
    let burger = <Spinner />;
    if (this.props.ingredients != null) {
      burger = <Burger ingredients={this.props.ingredients} />;
    }

    let orderSummary = <Redirect to="/" />;
    if (this.props.ingredients != null) {
      const redirect = this.props.purchased ? <Redirect to="/" /> : null;
      orderSummary = (
        <div>
          {redirect}
          <OrderSummary
            ingredients={this.props.ingredients}
            price={2.5}
            cancel={this.cancelHandler}
            continue={this.continuerHandler}
          />
          <Route path="/checkout/contact" component={ContactForm} />
        </div>
      );
    }

    return (
      <div className={classes.orderSummary}>
        <h2>Your order:</h2>
        {burger}
        {orderSummary}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ingredients: state.Burger.ingredients,
  purchased: state.Order.purchased
});

export default connect(mapStateToProps)(Checkout);
