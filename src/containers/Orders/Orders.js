import React, { Component } from "react";
import { connect } from 'react-redux'


import Spinner from "../../components/UI/Spinner/Spinner";
import Order from "../../components/Order/Order";
import {getOrders} from '../../store/actions/';

class Orders extends Component {
  
  componentDidMount() {
    console.log(this.props.token);
    this.props.onGetOrders(this.props.token);
  }

  render() {
    let orders = (
      <div>
        <p>Compiling a list of your previous orders</p>
        <Spinner />
      </div>
    );

    if (this.props.loading === false) {
      
      orders = this.props.orders.map(ing => (
        <Order
          key={ing.id}
          ing={ing.ingredients}
          price={ing.price}
        />
      ));
    }
    return (
      <div>
        {orders}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  orders: state.Order.orders,
  loading: state.Order.loading,
  token: state.Auth.token
})

const mapDispatchToProps = dispatch => ({
  onGetOrders: (token) => dispatch(getOrders(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(Orders);