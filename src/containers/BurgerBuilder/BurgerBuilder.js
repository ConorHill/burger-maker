import React, { Component } from "react";
import { connect } from "react-redux";

import Aux from "../../hoc/Auxillary";
import withError from "../../hoc/withError";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import Spinner from "../../components/UI/Spinner/Spinner";
import * as burgerActions from "../../store/actions/";

import axiosOrders from "../../axios-orders";

class BurgerBuilder extends Component {
  state = {
    orderNow: false,
    loading: false
  };

  componentDidMount() {
    this.props.onInitIngredients();
  }

  updatePriceState(ingredients) {
    const sum = Object.keys(ingredients)
      .map(key => ingredients[key])
      .reduce((cur, next) => cur + next, 0);
    return sum >= 2.0;
  }

  orderPurchaseHandler = () => {
    if (this.props.isAuth) {
      this.setState({ orderNow: true });
    } else {
      this.props.onAuthRedirect('/checkout');
      this.props.history.push("/auth");
    }
  };
  orderCancelHandler = () => {
    this.setState({ orderNow: false });
  };

  orderContinueHandler = () => {
    this.props.onStartPurchase();
    this.props.history.push("/checkout");
  };

  render() {
    const disabled = { ...this.props.ingredients };
    Object.keys(disabled).map(key => (disabled[key] = disabled[key] <= 0));

    let orderSummary = null;
    let burger = this.props.crash ? (
      <p>Burger Ingredients not available, please try again later</p>
    ) : (
      <Spinner />
    );
    if (this.props.ingredients && this.props.price) {
      burger = (
        <Aux>
          <Burger ingredients={this.props.ingredients} />
          <BuildControls
            addIngredient={this.props.onAddIngredient}
            removeIngredient={this.props.onDelIngredient}
            disabled={disabled}
            clicked={this.orderPurchaseHandler}
            purchaseable={this.updatePriceState(this.props.ingredients)}
            price={this.props.price}
            isAuth={this.props.isAuth}
          />
        </Aux>
      );
      orderSummary = (
        <OrderSummary
          cancel={this.orderCancelHandler}
          continue={this.orderContinueHandler}
          ingredients={this.props.ingredients}
          price={this.props.price}
        />
      );
    }

    if (this.state.loading) {
      orderSummary = <Spinner />;
    }

    return (
      <Aux>
        <Modal order={this.state.orderNow} clicked={this.orderCancelHandler}>
          {orderSummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

const mapStateToProps = state => ({
  ingredients: state.Burger.ingredients,
  price: state.Burger.totalPrice,
  error: state.Burger.error,
  crash: state.Burger.crash,
  isAuth: state.Auth.token !== null
});

const mapDispatchToProps = dispatch => ({
  onAddIngredient: ingName => {
    dispatch(burgerActions.addIngredient(ingName));
  },
  onDelIngredient: ingName => {
    dispatch(burgerActions.delIngredient(ingName));
  },
  onInitIngredients: () => {
    dispatch(burgerActions.initIngredients());
  },
  onStartPurchase: () => dispatch(burgerActions.checkout()),
  onAuthRedirect: (path) => dispatch(burgerActions.authRedirect(path))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withError(BurgerBuilder, axiosOrders));
