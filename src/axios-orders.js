import axios from 'axios';

const axiosOrders = axios.create({
    baseURL: 'https://burger-maker-88c66.firebaseio.com/'
});

export default axiosOrders;