import * as actionTypes from './actionTypes';
import axios from 'axios';

const authSuccess = (token, userID) => ({
  type: actionTypes.AUTH_SUCCESS,
  token: token, 
  userID: userID
})

const authFailure = (e) => ({
  type: actionTypes.AUTH_FAILURE,
  error: e
})

const authStart = ()  => ({
  type: actionTypes.START_AUTH
})

export const authLogout = () => ({
  type: actionTypes.AUTH_LOGOUT
})

export const authRedirect = (path) => ({
  type: actionTypes.AUTH_REDIRECT_PATH,
  path: path
})

const checkTimeout = (timeout) => {
  return dispatch => {
    setTimeout(() => {
      dispatch(authLogout)
    }, timeout * 1000);
  };
};

export const auth = (email, password, signUp) => {
  const authData = {
    email: email,
    password: password,
    returnSecureToken: true
  }
  let url = ''
  url = signUp ? 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyD_NWWWWIzcNniBgYVZ_5_pWz9iEtcie38' : 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyD_NWWWWIzcNniBgYVZ_5_pWz9iEtcie38'
  return dispatch => {
    dispatch(authStart());
    
    axios.post(url,authData)
    .then(res => {
      dispatch(authSuccess(res.data.idToken, res.data.localId));
      dispatch(checkTimeout(res.data.expiresInTime))
    })
    .catch(e => {
      dispatch(authFailure(e.response.data.error.message))
    });
  };
};