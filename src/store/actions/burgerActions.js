import * as actionTypes from "./actionTypes";
import axiosOrders from "../../axios-orders";

const INGREDIENT_PRICES = {
  salad: 0.6,
  cheese: 1.2,
  bacon: 1.6,
  meat: 2.0
};

export const addIngredient = ingName => ({
  type: actionTypes.ADD_INGREDIENT,
  ingName: ingName
});

export const delIngredient = ingName => ({
  type: actionTypes.DEL_INGREDIENT,
  ingName: ingName
});

const getIngredients = (ingredients, price) => ({
  type: actionTypes.GET_INGREDIENTS,
  ingredients: ingredients,
  price: price
});

const failGetIngredients = e => ({
  type: actionTypes.FAIL_GET_INGREDIENTS,
  error: e,
  crash: true
});

export const initIngredients = () => {
  return dispatch => {
    axiosOrders
      .get("/ingredients.json")
      .then(res => {
        let price = null;
        for (let ing in res.data) {
          price += INGREDIENT_PRICES[ing];
        }
        dispatch(getIngredients(res.data, price));
      })
      .catch(e => {
        dispatch(failGetIngredients(e));
      });
  };
};
