import * as actionTypes from "./actionTypes";
import axiosOrders from "../../axios-orders";

const orderSuccess = (id, data) => ({
  type: actionTypes.ORDER_SUCCESS,
  orderID: id,
  orderData: data
});

const orderFailure = e => ({
  type: actionTypes.ORDER_FAILURE,
  error: e
});

const startOrder = () => ({
  type: actionTypes.START_ORDER
});

export const checkout = () => ({
  type: actionTypes.CHECKOUT
});

export const sendOrder = (order,token) => {
  return dispatch => {
    dispatch(startOrder());
    axiosOrders
      .post("/orders.json?auth="+token, order)
      .then(res => {
        dispatch(orderSuccess(res.data.name, order));
      })
      .catch(err => {
        dispatch(orderFailure(err));
      });
  };
};

const getOrdersStart = () => ({
  type: actionTypes.START_GET_ORDERS
})

const getOrdersSuccess = (orders) => ({
  type: actionTypes.GET_ORDERS_SUCCESS,
  orders: orders
});

const getOrdersFailure = (e) => ({
  type: actionTypes.GET_ORDERS_FAILURE,
  error: e
});

export const getOrders = (token) => {
  return dispatch => {
    dispatch(getOrdersStart());
    const orders = [];
    axiosOrders
      .get("orders.json?auth=" + token)
      .then(res => {
        for (let key in res.data) {
          orders.push({ ...res.data[key], id:key});
        }
        dispatch(getOrdersSuccess(orders));
      })
      .catch(e => {
        dispatch(getOrdersFailure(e));
      });
  }
}