export const update = (old, updated) => ({
  ...old,
  ...updated
})