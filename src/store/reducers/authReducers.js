import * as actionTypes from "../actions/actionTypes";
import { update } from "../utilities";

const initialState = {
  token: null,
  userID: null,
  error: null,
  loading: false,
  path: "/"
};

const authStart = (state, action) => {
  return update(state, { error: null, loading: true });
};

const authSuccess = (state, action) => {
  return update(state, {
    error: null,
    token: action.token,
    userID: action.userID,
    loading: false
  });
};

const authFailure = (state, action) => {
  return update(state, { error: action.error, loading: false });
};

const authLogout = (state, action) => {
  return update(state, {
    error: null,
    loading: false,
    token: null,
    userID: null
  });
};

const authRedirect = (state, action) => {
  return update(state, { path: action.path });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.START_AUTH:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAILURE:
      return authFailure(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.AUTH_REDIRECT_PATH:
      return authRedirect(state, action);
    default:
      return state;
  }
};

export default reducer;
