import * as actionType from "../actions/actionTypes";
import { update } from "../utilities";

const INGREDIENT_PRICES = {
  salad: 0.6,
  cheese: 1.2,
  bacon: 1.6,
  meat: 2.0
};

const initialState = {
  ingredients: null,
  totalPrice: 2.0,
  error: "",
  crash: false,
  building: false
};

const getIngredients = (state, action) => {
  return update(state, {
    ingredients: action.ingredients,
    totalPrice: action.price,
    building: false
  });
};

const failGetIngredients = (state, action) => {
  return update(state, {
    error: action.error,
    crash: action.crash
  });
};

const addIngredient = (state, action) => {
  const updatedIngredients = update(state.ingredients, {
    [action.ingName]: state.ingredients[action.ingName] + 1
  });
  const updatedPrice = state.totalPrice + INGREDIENT_PRICES[action.ingName];
  return update(state, {
    ingredients: updatedIngredients,
    totalPrice: updatedPrice,
    building: true
  });
};

const delIngredient = (state, action) => {
  const updatedIngredients = update(state.ingredients, {
    [action.ingName]: state.ingredients[action.ingName] - 1
  });
  const updatedPrice = state.totalPrice - INGREDIENT_PRICES[action.ingName];
  return update(state, {
    ingredients: updatedIngredients,
    totalPrice: updatedPrice,
    building: true
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_INGREDIENTS:
      return getIngredients(state, action);
    case actionType.FAIL_GET_INGREDIENTS:
      return failGetIngredients(state, action);
    case actionType.ADD_INGREDIENT:
      return addIngredient(state, action);
    case actionType.DEL_INGREDIENT:
      return delIngredient(state, action);
    default:
      return state;
  }
};

export default reducer;
