import * as actionTypes from '../actions/actionTypes';
import {update} from '../utilities';

const initialState = {
    orders: [],
    loading: false,
    purchased: false,
    error: ''
};

const checkout = (state, action) => {
    return update(state,{purchased: false})
}

const startOrder = (state, action) => {
    return update(state, {loading: true})
}

const orderSuccess = (state, action) => {
    const newOrder = {
        id: action.orderID,
        orderData: action.orderData
    }
    return update(state, {
        loading: false,
        purchased: true,
        orders: state.orders.concat(newOrder)
    })
}

const orderFailure = (state, action) => {
    return update(state, {
        loading: false,
        error: action.error
    })
}

const startGetOrders = (state, action) => {
    return update(state, {loading: true})
}

const getOrdersSuccess = (state, action) => {
    return update(state, {
        loading: false,
        orders: action.orders
    })
}

const getOrdersFailure = (state, action) => {
    return update(state, {
        loading: false,
        error: action.error
    })
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.CHECKOUT: 
            return checkout(state, action)
        case actionTypes.START_ORDER:
            return startOrder(state, action)
        case actionTypes.ORDER_SUCCESS: 
            return orderSuccess(state, action)
        case actionTypes.ORDER_FAILURE:
            return orderFailure(state, action)
        case actionTypes.START_GET_ORDERS:
            return startGetOrders(state, action)
        case actionTypes.GET_ORDERS_SUCCESS:
            return getOrdersSuccess(state, action)
        case actionTypes.GET_ORDERS_FAILURE:
            return getOrdersFailure(state, action)
        default:
            return state
    }
}

export default reducer;