import React from "react";

import NavItem from "./NavItem/NavItem";
import classes from "./NavItems.css";

const navItems = props => (
  <ul className={classes.nav}>
    <NavItem link="/" exact>
      Burger Builder
    </NavItem>
    {props.isAuth ? <NavItem link="/orders">Orders</NavItem> : null}

    {!props.isAuth ? (
      <NavItem link="/auth">Auth</NavItem>
    ) : (
      <NavItem link="/logout">Log Out</NavItem>
    )}
  </ul>
);

export default navItems;
