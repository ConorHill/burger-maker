import React from "react";


import Logo from "../../Logo/Logo";
import NavItems from "../NavItems/NavItems";
import Hamburger from '../Hamburger/Hamburger';

import classes from "./Toolbar.css";



const toolbar = props => (
  <header className={classes.toolbar}>
    <Hamburger clicked={props.showSide}/>
    <Logo />
    <nav className={classes.desktopOnly}>
      <NavItems isAuth={props.isAuth}/>
    </nav>
  </header>
);

export default toolbar;
