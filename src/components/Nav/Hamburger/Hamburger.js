import React from 'react'

import classes from './Hamburger.css'

const hamburger = props => (
    <div className={classes.container} onClick={props.clicked}>
    <div className={classes.hamburger}></div>
    </div>
)

export default hamburger;