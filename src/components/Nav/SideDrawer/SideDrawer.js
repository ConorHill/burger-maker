import React from "react";

import NavItems from "../NavItems/NavItems";
import Logo from "../../Logo/Logo";
import Backdrop from "../../UI/Backdrop/Backdrop";
import Aux from "../../../hoc/Auxillary";

import classes from "./SideDrawer.css";
const sideDrawer = props => {
  let activeClasses = [classes.sideDrawer, classes.close];
  if (props.visible) activeClasses = [classes.sideDrawer, classes.open];

  return (
    <Aux>
      <Backdrop show={props.visible} clicked={props.click} />
      <div className={activeClasses.join(' ')}>
        <div className={classes.LogoHeight}>
          <Logo />
        </div>
        <nav>
          <NavItems />
        </nav>
      </div>
    </Aux>
  );
};

export default sideDrawer;
