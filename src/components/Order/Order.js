import React from "react";

import classes from "./Order.css";

export default function order(props) {
  let ingredients = [];
  for (let ingredientName in props.ing) {
    ingredients.push({
      name: ingredientName,
      amount: props.ing[ingredientName]
    });
  }

  let output = ingredients.map(ingredient => (
    <span className={classes.ingredient} key={ingredient.name}>
      {ingredient.name} ({ingredient.amount})
    </span>
  ));

  return (
    <div className={classes.orderContainer}>
      <div>
        <h3>Ingredients:</h3>
        <p>{output}</p>
      </div>
      <div>
        <h4>Price:</h4> <p> <strong>€{Number.parseFloat(props.price).toFixed(2)}</strong></p>
      </div>
    </div>
  );
}
