import React from 'react';

import classes from './Toolbar.css';
const toolbar = (props) => (
    <header className={classes.toolbar}>
        <div>Menu</div>
        <div>Logo</div>
        <nav><ul>List</ul></nav>
    </header>
);

export default toolbar;