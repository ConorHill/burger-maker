import React, { Component } from "react";
import {connect} from 'react-redux';
import Aux from "../../hoc/Auxillary";

import Toolbar from "../Nav/Toolbar/Toolbar";
import SideDrawer from "../Nav/SideDrawer/SideDrawer";

class Layout extends Component {
  state = {
    showSideDrawer: false
  };

  showSideDrawerHandler = () => {
      this.setState({ showSideDrawer: !this.state.showSideDrawer })
  };

  render() {
    return (
      <Aux>
        <Toolbar showSide={this.showSideDrawerHandler} isAuth={this.props.isAuth}/>
        <SideDrawer
          visible={this.state.showSideDrawer}
          click={this.showSideDrawerHandler}
        />
        <main>{this.props.children}</main>
      </Aux>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuth: state.Auth.token !== null
})

export default connect(mapStateToProps)(Layout);
