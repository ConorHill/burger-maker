import React from "react";

import Aux from "../../../hoc/Auxillary";
import Button from "../../UI/Button/Button";

const orderSummary = props => {
  const ingSummary = Object.keys(props.ingredients).map(key => (
    <li key={key}>
      <span style={{ textTransform: "capitalize" }}>{key}</span>:{" "}
      {props.ingredients[key]}
    </li>
  ));
  return (
    <Aux>
      <h3>Your Order:</h3>
      <ul>{ingSummary}</ul>
      <h4>€{props.price.toFixed(2)}</h4>
      <p>Continue to checkout?</p>
      <hr />
      <Button btnType="stop" clicked={props.cancel}>
        Cancel
      </Button>
      <Button btnType="continue" clicked={props.continue}>
        Continue
      </Button>
    </Aux>
  );
};

export default orderSummary;
