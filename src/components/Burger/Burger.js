import React from 'react';

import classes from './Burger.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props) => {
    let transIngredients = Object.keys(props.ingredients)
        .map(ingredient => {
            return [...Array(props.ingredients[ingredient])]
            .map((_, i) => {
                return <BurgerIngredient key={ingredient + i} type={ingredient} />;
            });
        })
        .reduce((pre, cur) => {
            return pre.concat(cur);
        }, []);

    if(transIngredients.length === 0){
        transIngredients = <p>Please add some ingredients to your burger.</p>;
    }
    return (
        <div className={classes.burger}>
            <BurgerIngredient type="bread-top" />
            {transIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    )
}

export default burger;