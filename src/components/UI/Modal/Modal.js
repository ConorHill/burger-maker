import React from "react";

import Aux from "../../../hoc/Auxillary";
import classes from "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";

const modal = props => (
  <Aux>
      <Backdrop show={props.order} clicked={props.clicked}/>
  <div
    style={{
      display: props.order ? "block" : "none"
    }}
    className={classes.modal}
  >
    {props.children}
  </div>
  </Aux>
);

export default modal;
