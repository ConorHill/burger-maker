import React from "react";

import classes from "./Input.css";

const input = props => {
  let inputElemets = "";
  let inputClasses = [classes.input];

  if (props.invalid && props.touched) {
    inputClasses.push(classes.invalid);
  }

  switch (props.elementType) {
    case "input":
      inputElemets = (
        <input
          type="input"
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.change}
        />
      );
      break;
    case "textarea":
      inputElemets = (
        <textarea
          cols="2"
          rows="2"
          value={props.value}
          onChange={props.change}
        />
      );
      break;
    case "select":
      inputElemets = (
        <select
          value={props.value}
          onChange={props.change}
          className={classes.input}
        >
          {props.elementConfig.options.map(i => (
            <option key={i.value} value={i.value}>
              {i.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElemets = <p>Not a valid input object</p>;
  }

  return <div>{inputElemets}</div>;
};

export default input;
