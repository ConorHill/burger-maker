import React from 'react'

import BurgerLogo from '../../assests/images/burger-logo.png';
import classes from '../Logo/Logo.css';

const logo = () => (
    <div className={classes.logoContainer}>
        <img className={classes.logo}
        src={BurgerLogo} alt="Burger Logo"/>
    </div>
);

export default logo;